# Raft implementation in Pony

The objective of this project is to start bootstrapping a Raft implementation using Pony. More specifically, and since I haven't yet decided whether I want to use Pony or not for the full implementation of the algorithm, in this repository I'll be focusing only on the leader election part of Raft.

Together with this repository, there will be other ones where I'll be experimenting with other programming languages until I find the one that better fits what I expect from it [the language] so that I can continue with the full implementation.

## What is here to see?

For running the project just compile the `main.pony` file using the following command on the root folder:

```
$ ponyc -d
```

After that, you can just run the binary file:

```
$ ./raft-pony
```

In `main.pony` you'll find the main procedure (as the name suggest) while on `raft.pony` you'll encounter the "meat" of the project.

## Other implementations

- [Ada](https://gitlab.com/jdi/raft-ada)
- [Go](https://gitlab.com/jdi/raft-go)
