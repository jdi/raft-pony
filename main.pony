use "random"
use "time"
use "collections"

actor Main
  new create(env: Env) =>
    let delay: I64 = 1
    let rand: Rand = Rand(Time.now()._1.u64())

    let s1 = Server(1, env)
    let s2 = Server(2, env)
    let s3 = Server(3, env)
    let s4 = Server(4, env)
    let s5 = Server(5, env)

    let c =
        recover val
            var cluster: MapIs[I64, Server] = MapIs[I64, Server]
            try
                cluster.insert(1, s1)?
                cluster.insert(2, s2)?
                cluster.insert(3, s3)?
                cluster.insert(4, s4)?
                cluster.insert(5, s5)?
            end
            cluster
        end

    s1.configure(c)
    s2.configure(c)
    s3.configure(c)
    s4.configure(c)
    s5.configure(c)

    s1.start()
    s2.start()
    s3.start()
    s4.start()
    s5.start()

    @sleep[I64](delay)
    s4.issue_command()
    @sleep[I64](delay)
    s4.issue_command()
    @sleep[I64](delay)
    s1.sleep(1)
    s3.sleep(1)
    @sleep[I64](delay)
    @sleep[I64](delay)
    s4.issue_command()

    @sleep[I64](delay)
    s1.print_log()
    @sleep[I64](delay)
    s2.print_log()
    @sleep[I64](delay)
    s3.print_log()
    @sleep[I64](delay)
    s4.print_log()
    @sleep[I64](delay)
    s5.print_log()
    @sleep[I64](delay)
