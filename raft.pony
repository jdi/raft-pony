use "collections"
use "time"
use "random"
use "logger"

primitive Follower fun apply(): String => "follower"
primitive Candidate fun apply(): String => "candidate"
primitive Leader fun apply(): String => "leader"

type RoleType is (Follower | Candidate | Leader)

actor Server
    let id: I64
    var current_term: I64 = 0
    var role: RoleType = Follower
    var voted_for: (I64 | None) = None
    var current_leader: (I64 | None) = None
    var commit_index: I64 = 0
    var log: Array[I64] = []
    var peers: MapIs[I64, Server] = MapIs[I64, Server]

    let logger: Logger[String]
    let timers: Timers = Timers

    var configured: Bool = false
    var started: Bool = false

    // Volatile on leaders
    var next_index: MapIs[I64, USize] = MapIs[I64, USize]

    // Volatile after every election
    var number_of_votes: I64 = 1

    new create(id': I64, env: Env) =>
        id = id'
        logger = Logger[String](Info, env.out, {(msg: String) => "[SERVER " + id.string() + "] " + msg}, ServerLogFormatter)
        try
            log.insert(0, 0)?
        else
            logger.log("couldn't initialize log.")
        end

    be configure(peers': MapIs[I64, Server] val) =>
        var c: Bool = true
        for peer_id in peers'.keys() do
            match peer_id == id
            | true => None
            | false =>
                try
                    peers.insert(peer_id, peers'(peer_id)?)?
                else
                    logger.log("couldn't insert peer " + peer_id.string())
                    c = false
                end
            end
        end
        configured = c

    be start() =>
        match configured
            | true =>
                set_timers()
                started = true
            | false => logger.log("is not configured.")
        end

    be start_election() =>
        if role.apply() == Follower.apply() then
            current_term = current_term + 1
            role = Candidate
            number_of_votes = 1
            logger.log("started an election. Term: " + current_term.string())
            try
                let last_log_index: USize = get_last_log_index()
                let last_log_term: I64 = get_last_log_term()?
                for p in peers.values() do
                    p.request_vote(current_term, id, last_log_index, last_log_term)
                end
            else
                logger.log("couldn't proceed with the election.")
            end
        end

    be send_heartbeats() =>
        try
            if role.apply() == Leader.apply() then
                for peer_id in peers.keys() do
                    let ni = next_index(peer_id)?
                    let prev_log_index = get_prev_log_index(ni)
                    let prev_log_term = get_prev_log_term(ni)?
                    let entries = get_entries(ni)?
                    peers(peer_id)?.append_entries(current_term, id, prev_log_index, prev_log_term, consume entries, commit_index)
                end
            end
        end

    be request_vote(term: I64, candidate_id: I64, last_log_index: USize, last_log_term: I64) =>
        if not started then
            logger.log("not started.")
            return
        end
        try
            if (term <= current_term) or not (voted_for is None) then
                peers(candidate_id)?.request_vote_response(current_term, false)
                logger.log("denied its vote to Server " + candidate_id.string())
                return
            end
            if (last_log_index < get_last_log_index()) or (last_log_term < get_last_log_term()?) then
                peers(candidate_id)?.request_vote_response(current_term, false)
                logger.log("denied its vote to Server " + candidate_id.string())
                return
            end
            logger.log("granted its vote to Server " + candidate_id.string())
            voted_for = candidate_id
            current_term = term
            role = Follower
            set_timers()
            peers(candidate_id)?.request_vote_response(current_term, true)
        else
            logger.log("couldn't process request_vote.")
        end

    be request_vote_response(term: I64, vote_granted: Bool) =>
        if term > current_term then
            role = Follower
            current_term = term
            return
        end
        if vote_granted then
            number_of_votes = number_of_votes + 1
        end
        if number_of_votes.usize() > (peers.size() / 2) then
            role = Leader
            current_leader = None
            set_next_index()
        end

    be append_entries(term: I64, leader_id: I64, prev_log_index: USize, prev_log_term: I64, entries: Array[I64] iso, leader_commit: I64) =>
        if not started then
            logger.log("not started.")
            return
        end
        try
            if term < current_term then
                logger.log("rejected a heartbeat from Server " + leader_id.string())
                peers(leader_id)?.append_entries_response(current_term, false, id)
                return
            end
            if prev_log_term != log(prev_log_index)? then
                logger.log("rejected a heartbeat from Server " + leader_id.string())
                peers(leader_id)?.append_entries_response(current_term, false, id)
                return
            end
            logger.log("received a heartbeat from Server " + leader_id.string())
            current_leader = leader_id
            current_term = term
            voted_for = None
            role = Follower
            update_log(prev_log_index, consume entries)
            peers(leader_id)?.append_entries_response(current_term, true, id)
            set_timers()
        end

    be append_entries_response(term: I64, success: Bool, peer_id: I64) =>
        if term > current_term then
            role = Follower
            current_term = term
        end
        if success then
            update_next_index(peer_id)
        end

    fun get_last_log_index(): USize =>
        log.size() - 1

    fun get_last_log_term(): I64? =>
        log(get_last_log_index())?

    fun get_prev_log_index(ni: USize): USize =>
        if (log.size() == 1) or (ni < 1) then
            return 0
        end
        ni - 1

    fun get_prev_log_term(ni: USize): I64? =>
        log(get_prev_log_index(ni))?

    fun ref set_next_index() =>
        for peer_id in peers.keys() do
            next_index.update(peer_id, log.size())
        end

    fun ref update_next_index(peer_id: I64) =>
        next_index.update(peer_id, log.size())

    fun get_entries(ni: USize): Array[I64] iso^? =>
        var result = recover Array[I64] end
        var i: USize = ni
        while i < log.size() do
            result.push(log(ni)?)
            i = i + 1
        end
        result

    fun ref update_log(prev_log_index: USize, entries: Array[I64] iso) =>
        if entries.size() == 0 then
            return
        end
        let es = recover ref consume entries end
        let index: USize = prev_log_index + 1
        log.remove(index, log.size() - 1)
        for entry in es.values() do
            log.push(entry)
        end

    fun set_timers() =>
        timers.dispose()
        let seed: Rand = Rand(Time.now()._2.u64())
        let dice: Dice = Dice(seed)

        let min_election_duration: F64 = 350_000_000
        let min_heartbeat_duration: F64 = (min_election_duration / 10)
        let roll_dice: F64 = dice.apply(1, 100).f64() / 100
        let election_duration: U64 = ((roll_dice * min_election_duration) + min_election_duration).u64()
        let heartbeats_duration: U64 = ((roll_dice * min_heartbeat_duration) + min_heartbeat_duration).u64()

        let election_timer = Timer(ElectionTimer(this), election_duration, election_duration)
        let heartbeats_timer = Timer(HeartbeatTimer(this), heartbeats_duration, heartbeats_duration)

        timers(consume election_timer)
        timers(consume heartbeats_timer)

    be issue_command() =>
        if role.apply() == Leader.apply() then
            log.push(current_term)
            logger.log("command issued.")
            send_heartbeats()
        else
            match current_leader
            | None => None
            | let cl: I64 => try peers(cl)?.issue_command() end
            end
        end

    be print_log() =>
        logger.log("{")
        for e in log.values() do
            logger.log(" {Term: " + e.string())
        end
        logger.log("}")

    be sleep(delay: I64) =>
        @sleep[I64](delay)

// Defines the timeout for the elections.
class ElectionTimer is TimerNotify
  let server: Server

  new iso create(s: Server) =>
    server = s

  fun ref apply(timer: Timer, count: U64): Bool =>
    server.start_election()
    true

// Defines the timeout for the heartbeats.
class HeartbeatTimer is TimerNotify
  let server: Server

  new iso create(s: Server) =>
    server = s

  fun ref apply(timer: Timer, count: U64): Bool =>
    server.send_heartbeats()
    true

primitive ServerLogFormatter is LogFormatter
  fun apply(msg: String, loc: SourceLoc): String =>
    msg
